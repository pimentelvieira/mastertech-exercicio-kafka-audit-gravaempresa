package com.mastertech.gravaempresa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MastertechExercicioKafkaAuditGravaempresaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MastertechExercicioKafkaAuditGravaempresaApplication.class, args);
	}

}
