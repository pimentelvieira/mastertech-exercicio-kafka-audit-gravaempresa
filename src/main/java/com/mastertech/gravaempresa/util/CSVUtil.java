package com.mastertech.gravaempresa.util;

import com.mastertech.cadempresa.models.Empresa;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CSVUtil {

    public static String convertToCSV(Empresa data) {
        String [] array = new String[2];
        array[0] = data.getNome();
        array[1] = data.getCnpj();

        return Stream.of(array)
                .collect(Collectors.joining(","));
    }

    public static void gravaNoCSV(String data) throws IOException {
        String fileName = "/home/a2/Documentos/csvfiles/empresas.csv";
        File file = new File(fileName);

        if (!file.exists()) {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            writer.write(data);
            writer.newLine();
            writer.close();
        } else {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
            writer.append(data);
            writer.newLine();
            writer.close();
        }
    }
}
