package com.mastertech.gravaempresa.consumers;

import com.mastertech.cadempresa.models.Empresa;
import com.mastertech.gravaempresa.util.CSVUtil;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class EmpresaConsumer {

    @KafkaListener(topics = "spec2-william-pimentel-2", groupId = "gravagroup")
    public void receber(@Payload Empresa empresa) throws IOException {
        System.out.println("Inicio gravação arquivo, empresa: " + empresa.getNome());
        CSVUtil.gravaNoCSV(CSVUtil.convertToCSV(empresa));
        System.out.println("Fim gravação arquivo, empresa: " + empresa.getNome());
    }
}
